import { isValidFormula, getChunks, isValidCell, isValidExcelCell } from './utils';

const allowed = ['A1', 'B1', 'C1', 'D1'];
describe('isValidCell', () => {
  it('should allow values from the allowed list', () => {
    expect(isValidCell('A1', allowed)).toBe(true);
  });
  it('should reject values not in the allowed list', () => {
    expect(isValidCell('F1', allowed)).toBe(false);
  });
});

describe('isValidFormula', () => {
  it('should reject if does not begin with equals', () => {
    expect(isValidFormula('SUM(A1,B1)', allowed)).toBe(false);
  });
  it('should accept if equal number of opening and closing braces', () => {
    expect(isValidFormula('=SUM((A1,B1))', allowed)).toBe(true);
  });
  it('should reject if does not end with a closing bracket', () => {
    expect(isValidFormula('=SUM(A1,B1', allowed)).toBe(false);
  });

  it('should reject if unequal number of opening and closing braces', () => {
    expect(isValidFormula('=SUM(A1,B1))', allowed)).toBe(false);
  });

  it('should reject if it contains any characters other than comma and brackets, alphabets and one', () => {
    expect(isValidFormula('=!$@', allowed)).toBe(false);
    expect(isValidFormula('=SUM{A1,B1}', allowed)).toBe(false);
    expect(isValidFormula('=SUM(A2,B1)', allowed)).toBe(false);
    expect(isValidFormula('=SUM<(A1,B1', allowed)).toBe(false);
  });

  it('should reject if cells not from the allowed list', () => {
    expect(isValidFormula('=SUM(A2,F1)', allowed)).toBe(false);
    expect(isValidFormula('=SUM(A1,E1)', allowed)).toBe(false);
    expect(isValidFormula('=SUM(ZZ1,E1)', allowed)).toBe(false);
    expect(isValidFormula('=SUM(ZZ,E)', allowed)).toBe(false);
    expect(isValidExcelCell('=SUM(A1,MULTIPLY)', allowed)).toBe(false);
  });

  it('should accept if cells are from the allowed list', () => {
    expect(isValidFormula('=SUM(A1,B1)', allowed)).toBe(true);
    expect(isValidFormula('=SUM(B1,B1)', allowed)).toBe(true);
    expect(isValidFormula('=SUM(D1,C1)', allowed)).toBe(true);
  });

  it('should have only one equal', () => {
    expect(isValidFormula('=SUM(A1,B1,=MULTIPLY(A1,C1))', allowed)).toBe(false);
  });
});


describe('isValidExcelCell', () => {
  it('should pass for random test cases', () => {
    expect(isValidExcelCell('A1', allowed)).toBe(true);
    expect(isValidExcelCell('E1', allowed)).toBe(false);
    expect(isValidExcelCell('=SUM1', allowed)).toBe(false);
    expect(isValidExcelCell('=SUM(', allowed)).toBe(false);
    expect(isValidExcelCell('=SUM()', allowed)).toBe(false);
    expect(isValidExcelCell('=SUM(A1,MULTIPLY)', allowed)).toBe(false);
    expect(isValidExcelCell('=SUM(A1,MULTIPLY(A1,SUM(A1,B1)))', allowed)).toBe(true);
  });
});

describe('getChunks', () => {
  it('should get chunks', () => {
    expect(getChunks('MULTIPLY(A1,B1)')).toEqual(['A1', 'B1']);
    expect(getChunks('MULTIPLY(A1,MULTIPLY)')).toEqual(['A1', 'MULTIPLY']);
    expect(getChunks('MULTIPLY(A1,MULTIPLY(B1))')).toEqual(['B1', 'A1']);
    expect(getChunks('MULTIPLY(A1,MULTIPLY(B1,C1))')).toEqual(['B1', 'C1', 'A1']);
    expect(getChunks('MULTIPLY(A1,MULTIPLY(B1,MULTIPLY(A1,D1)))')).toEqual(['A1', 'D1', 'B1', 'A1']);
    expect(getChunks('')).toEqual([]);
  });
});
