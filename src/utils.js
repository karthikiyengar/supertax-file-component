import S from 'string';

export const isValidCell = (value, allowed) => allowed.indexOf(value) > -1;

export const getChunks = (string) => {
  if (string.indexOf('(') === -1 && string.indexOf(')') === -1) {
    return [];
  }
  const between = string.slice(string.indexOf('(') + 1, string.lastIndexOf(')'));
  return getChunks(between)
    .concat(between
      .split(',')
      .filter(item => item.indexOf('(') === -1 && item.indexOf(')') === -1));
};

export const isValidFormula = (value, allowed) => {
  if (!value.match(/^\=[A-Za-z(1,)]*\)$/)) { //eslint-disable-line
    return false;
  }
  const input = S(value);
  if (input.count('(') !== input.count(')')) {
    return false;
  }
  const chunks = getChunks(value);
  for (let i = 0; i < chunks.length; i += 1) {
    if (allowed.indexOf(chunks[i]) === -1) {
      return false;
    }
  }
  return true;
};

export const isValidExcelCell = (value, allowed = []) => {
  const input = S(value);
  if (input.startsWith('=')) {
    return isValidFormula(value, allowed);
  }
  return isValidCell(value, allowed);
};

export const getLastPart = (value) => {
  const regexp = /([^(|^=|^\,]*$)/gi; //eslint-disable-line
  const groups = regexp.exec(value);
  return groups
    ? groups[groups.length - 1]
    : '';
};
