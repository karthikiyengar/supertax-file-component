/* eslint max-len: "off" */
/* global FormData */
import React, { Component } from 'react';
import Dropzone from 'react-dropzone';
import Autocomplete from 'react-autocomplete';
import S from 'string';
import request from 'axios';
import cx from 'classnames';
import './App.css';
import { isValidExcelCell, getLastPart } from './utils';

request.defaults.baseURL = 'http://localhost:8080';

const matchItemToTerm = (option, string, allowed) => {
  if (option.value === '') { // placeholder
    return string === '';
  }
  const input = S(string.toLowerCase());
  const lastPart = S(getLastPart(string.toLowerCase()));
  const label = S(option.label.toLowerCase());
  const value = S(option.value.toLowerCase());
  const charBeforeLastPart = S(input.charAt(input.indexOf(lastPart) - 1));
  const partBeforeComma = string.slice(string.lastIndexOf('(') + 1, string.lastIndexOf(','));
  const isPartBeforeCommaValid = partBeforeComma.split(',').every(item => allowed.includes(item));
  const showFormula = input.s === '=' ||
    input.endsWith('(') ||
    [',', '(', '='].includes(charBeforeLastPart.s) ||
    (input.startsWith('=') && isPartBeforeCommaValid);

  const showCells = !input.startsWith('=') ||
    input.endsWith('(') || charBeforeLastPart.s === '(' || isPartBeforeCommaValid;

  if (option.isFormula && showFormula) {
    return label.contains(lastPart);
  }
  if (!option.isFormula && showCells) {
    return label.contains(lastPart) || value.startsWith(lastPart);
  }
  return false;
};

class App extends Component {
  constructor() {
    super();
    this.state = {
      filename: null,
      uploading: false,
      template: '',
      uploaded: false,
      headers: [],
      uploadError: null,
      submitError: null,
      formulae: [],
      fields: [],
      fieldErrors: {},
      fieldValues: {},
    };
    this.handleFileDrop = this.handleFileDrop.bind(this);
    this.handleFieldChange = this.handleFieldChange.bind(this);
    this.getItems = this.getItems.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    request.get('/api/formulae')
      .then((res) => {
        this.setState({ formulae: res.data });
      });

    request.get('/api/fields')
      .then((res) => {
        this.setState({ fields: res.data });
      });
  }

  getItems() {
    const placeholder = { label: 'Enter custom formula (=)', value: '' };
    return [placeholder, ...this.state.formulae, ...this.state.headers];
  }

  handleFieldChange(key, value, allowed) {
    const fieldErrors = { ...this.state.fieldErrors };
    console.log(value, allowed, isValidExcelCell(value, allowed));
    if (!isValidExcelCell(value, allowed)) {
      fieldErrors[key] = value;
    } else {
      delete fieldErrors[key];
    }
    this.setState({
      fieldValues: {
        ...this.state.fieldValues,
        [key]: value,
      },
      fieldErrors,
    });
  }

  handleSelect(key, value, allowed) {
    const currentValue = S(this.state.fieldValues[key] || '');
    const lastPart = getLastPart(currentValue);
    let completed;
    if (currentValue.startsWith('=')) {
      if (lastPart === '') {
        completed = currentValue + value;
      } else {
        completed = currentValue.replace(lastPart, value);
      }
    } else {
      completed = value;
    }
    this.handleFieldChange(key, completed, allowed);
  }

  handleSubmit() {
    request.post('/api/submit', {
      filename: this.state.filename,
      template: this.state.template,
      payload: this.state.fieldValues,
    }).then(() => {
      this.setState({
        submitError: null,
      });
    }).catch((err) => {
      this.setState({
        submitError: err.response ? err.response.data.message : err.message,
      });
    });
  }

  handleFileDrop(acceptedFiles) {
    const data = new FormData();
    data.append('file', acceptedFiles[0]);
    this.setState({ uploading: true });
    request.post('/api/upload', data)
      .then((res) => {
        this.setState({
          filename: res.data.filename,
          uploading: false,
          uploaded: true,
          headers: res.data.headers,
          fieldValues: {},
          fieldErrors: {},
        });
      }).catch((err) => {
        this.setState({
          uploadError: err.response ? err.response.data.message : err.message,
        });
      });
  }

  render() {
    const { uploadError, submitError, filename, uploading, uploaded, headers, fields, fieldValues, fieldErrors } = this.state;
    const allowed = headers.map(item => item.value);
    return (
      <div className="app">
        <div className="header" />
        <div className="card">
          <h1>Map Fields</h1>
          <div className="form-control">
            <label htmlFor="template">Template Name</label>
            <input name="template" type="text" onChange={e => this.setState({ template: e.target.value })} value={this.state.template} />
          </div>
          <div className="form-control">
            <label htmlFor="dropzone">Upload CSV</label>
            <div className="dropzone-container">
              <Dropzone onDrop={this.handleFileDrop} multiple={false} accept="text/csv" className="dropzone" disableClick={this.state.uploading}>
                {uploading ? 'Loading' : 'Click or Drag Files Here to Upload'}
              </Dropzone>
              <span className="filename">
                {filename}
              </span>
            </div>
          </div>
          {uploadError && <span className="error">{uploadError}</span>}
          {uploaded && headers.length > 0 &&
          <div className="table-container">
            <h3>Headers of CSV Uploaded</h3>
            <table>
              <thead>
                <tr>
                  <th /> {headers.map(item => (
                    <th key={item.value}>{item.value.slice(0, item.value.length - 1)}</th>
                  ))}
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1</td>
                  {headers.map(item => (
                    <td key={item.value}>{item.label}</td>
                  ))}
                </tr>
              </tbody>
            </table>
          </div>}
          {uploaded &&
          <div className="table-container">
            <table className="borderless">
              <thead>
                <tr>
                  <th>Supertax Fields</th>
                  <th>Uploaded CSV Fields</th>
                </tr>
              </thead>
              <tbody>
                { fields.map(field => (
                  <tr>
                    <td>{field.label}</td>
                    <td>
                      <div className="autocomplete-wrapper">
                        <Autocomplete
                          value={fieldValues[field.value] || ''}
                          inputProps={{
                            name: field.value,
                            className: cx(fieldErrors[field.value] ? 'input-error' : ''),
                            placeholder: 'Select / Enter field',
                          }}
                          items={this.getItems()}
                          getItemValue={item => item.value}
                          shouldItemRender={(option, string) => matchItemToTerm(option, string, allowed)}
                          onChange={(e, inputVal) => this.handleFieldChange(field.value, inputVal, allowed)}
                          onSelect={selectedVal => this.handleSelect(field.value, selectedVal, allowed)}
                          renderItem={(item, isHighlighted) => (
                            <div className={cx('autocomplete-item', { highlighted: isHighlighted })} key={item.value}>
                              <span>{item.label}</span>
                              {!item.isFormula && <span>{item.value}</span> }
                            </div>
                          )}
                          renderMenu={(items => <div className="autocomplete-menu">
                            {items.slice(0, 7)}
                          </div>)}
                        />
                      </div>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
            <div className="button-container">
              <button className="button" onClick={this.handleSubmit} disabled={Object.keys(fieldErrors).length > 0 || Object.keys(fieldValues).length !== fields.length}>Submit</button>
              {submitError && <div className="error">{submitError}</div>}
            </div>
          </div> }
        </div>
      </div>
    );
  }
}

export default App;
