const winston = require('winston');
const restify = require('restify');
const csv = require('csvtojson');
const Bluebird = require('bluebird');
const readFile = Bluebird.promisify(require('fs').readFile);
const writeFile = Bluebird.promisify(require('fs').writeFile);
const converter = require('number-converter-alphabet');

winston.level = 'debug';

const server = restify.createServer();
server.use(restify.queryParser());
server.use(restify.bodyParser());
server.use(restify.CORS());

server.get('/api/ping', (req, res, next) => {
  res.send('pong');
  return next();
});

server.get('/api/formulae', (req, res, next) => {
  res.send(['MULTIPLY', 'SUM', 'DIVIDE'].map(item => ({ label: item, value: item, isFormula: true })));
  return next();
});

server.get('/api/fields', (req, res, next) => {
  res.send([
    {
      label: 'CGST Amount',
      value: 'cgst_amt',
    }, {
      label: 'Invoice Date',
      value: 'invoice_date',
    },
  ]);
  return next();
});

server.post('/api/submit', (req, res, next) => {
  // send stuff to your API here and return errors
  res.send(req.body);
  return next();
});

server.post('/api/upload', (req, res, next) => {
  readFile(req.files.file.path).then((data) => {
    const newPath = `${__dirname}/uploads/${req.files.file.name}`; // replace logic with wherever you want to store / cloud
    writeFile(newPath, data);
  }).then(() => {
    const headers = [];
    csv({ noheader: true })
      .fromFile(req.files.file.path)
      .on('csv', (row, rowNum) => {
        if (rowNum === 0) {
          row.forEach((item, index) => {
            const value = `${converter.default(index, converter.ALPHABET_ASCII, {
              implicitLeadingZero: false })}1`.toUpperCase();
            headers.push({ label: item, value });
          });
        }
      }).on('done', (e) => {
        if (e) {
          return next(e);
        }
        res.send({ filename: 'sample.csv', headers, status: 'success' });
        return next();
      });
  }).catch(e => next(e));
});

server.listen(8080, () => {
  winston.debug('%s listening at %s', server.name, server.url);
});
